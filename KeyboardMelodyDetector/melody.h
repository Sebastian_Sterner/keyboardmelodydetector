#ifndef MELODY_INCLUDED_H
#define MELODY_INCLUDED_H

#define NUM_OF_MELODY_TONES (8U)
#define STD_STATE_TIMEOUT   (10000U)

#define MELODY { \
  { \
    .u8_tone = TONE_C1, \
    .u16_state_timeout = STD_STATE_TIMEOUT, \
  }, \
  { \
    .u8_tone = TONE_D1, \
    .u16_state_timeout = STD_STATE_TIMEOUT, \
  }, \
  { \
    .u8_tone = TONE_C1, \
    .u16_state_timeout = STD_STATE_TIMEOUT, \
  }, \
  { \
    .u8_tone = TONE_A1, \
    .u16_state_timeout = STD_STATE_TIMEOUT, \
  }, \
  { \
    .u8_tone = TONE_C1, \
    .u16_state_timeout = STD_STATE_TIMEOUT, \
  }, \
  { \
    .u8_tone = TONE_D1, \
    .u16_state_timeout = STD_STATE_TIMEOUT, \
  }, \
  { \
    .u8_tone = TONE_C1, \
    .u16_state_timeout = STD_STATE_TIMEOUT, \
  }, \
  { \
    .u8_tone = TONE_A1, \
    .u16_state_timeout = STD_STATE_TIMEOUT, \
  } \
}

#endif
