# KeyboardMelodyDetector

Detect pressed keys on a astandard keyboard using scanning technique. A melody consisting of arbitrary consecutive tones can be configured and a relay is triggered, if the correct melody is detected.